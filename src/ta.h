/*
A terminal based audio player implemented with FFMPEG.
Copyright (C) 2018 Aaron Calder

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef AT_TA_TA
#define AT_TA_TA

/* File descriptors for the communication pipes.
 * 'ta' == main, 'in' == input, 'out' == output, 'plr' == player */
                       /* ___________________    */
int ta_read_from_in;   /* | read from input |  4 */
int in_write_to_ta;    /* |_________________|  5 */
int out_read_from_ta;  /* | write to output |  6 */
int ta_write_to_out;   /* | _ _ _ _ _ _ _ _ |  7 */
int out_read_from_plr; /* |  (from player)  |  8 */
int plr_write_to_out;  /* |_________________|  9 */
int plr_read_from_ta;  /* | write to player |  10 */
int ta_write_to_plr;   /* |_________________| 11 */

#endif /* AT_TA_TA */
